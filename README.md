Front End Engineering Test
====================

This is a memory match game I created over the course of the weekend. 

There are a few bugs to iron out such as the aside box needing move to a different location within the browser and making the cards responsive when the screen is minimized. 

I was however able to get the title bar to be responsive, the other elements we're giving me trouble. 

Thank you for giving me this opportunity and I look forward to hearing your response. 

Gawain Bracy II

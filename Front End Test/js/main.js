

(function($) {
	$.fn.memoryGame = function(options) {
		

		/* Variables ************************/

		//gets the parameters
		var opts = $.extend({}, $.fn.memoryGame.defaults, options);

		//keeps the number of cards
		var itemsNum = $(this).children('ul').children('li').length;

		//we keep the selected correct items here (After we have a match)
		var correctItems = [];

		//keeps the matching ids = which elements match
		var matches = [];

		//keep the inner html of the elements (to hide them from web inspector,etc)
		var inHTML = [];

		//a selector class for the cards
		var itemsClass = 'mg-item';

		//keep the class and the id of the selected item
		var selItemClass = '';
		var selItemId = -1;

		//keeps the number of clicks for a turn - it can be 0, 1 or 2
		var numClicks = 0;

		//keeps the number of matches and the number of seconds for the summary
		var numMatches = 0;
		var numSeconds = 0;

		//keeps the number of clicks in general
		var numTotalClicks = 0;

		//a timer variable
		var gameTimer;

		//additional variables for customization
		var delayShow = opts.openDelay;
		var w = opts.itemWidth;
		var h = opts.itemHeight;
		var m = opts.itemsMargin;
		var rowNum = Math.ceil(itemsNum/opts.colCount);

		/* FUNCTIONS *********************/

		//a function to handle the element click
		var handleClick = function(){
			//starts the timer
			if(numTotalClicks==0) gameTimer = setInterval(incTime,1000);
			//counts the clicks
			numTotalClicks ++;
			//keeps the class for the clicked item
			var tId = $(this).attr('id');
			var tdIdNum = parseInt(tId.substring(itemsClass.length,tId.length));
			var tClass = matches[tdIdNum];
			//calls the unbind function (makes the button inactive)
			unbindClick($(this));
			showItem($(this),tdIdNum);
			//if it's the first click out of two (turning the second card)
			if(numClicks==0){
				numClicks ++ ;
				selItemClass = tClass;
				selItemId = tId;
			//if it's the second click out of two (turning the second card)
			}else if(numClicks == 1){
				numClicks = 0;
				//if both have the same class = we have a match
				if(tClass == selItemClass){
					showResIcon('correct');
					unbindClick($('.'+tClass));
					//adds the turned cards to the correct items array
					correctItems.push(tId);
					correctItems.push(selItemId);
					//increases the number of correct matches
					numMatches ++ ;
					// if all of the cards are turned and the game is complete
					if(numMatches == itemsNum/2){
						//removes timer
						clearInterval(gameTimer);
						//if game summary is set, adds the info to it and shows it.
						if(opts.gameSummary){
							$('div#game-summary').
								children('div#gs-column2').
								html(numSeconds+'<br>'+opts.textSummaryTime);
							$('div#game-summary').
								children('div#gs-column3').
								html(numTotalClicks+'<br>'+opts.textSummaryClicks);
							$('div#game-summary').delay(2000).fadeIn(1000);
						}
						//makes an AJAX call and sends the necessary parameters
						if(opts.onFinishCall!=''){
							opts.onFinishCall({ clicks: numTotalClicks, time: numSeconds } );
						}
					}
				//if they dont have the same class = We Dont Have a match
				}else{
					showResIcon('wrong');
					unbindClick($('div.'+itemsClass));
					//turns the cards back
					hideItem($('div#'+selItemId));
					hideItem($(this));
					//after a certain time adds back the click event to the card
					setTimeout(function(){
						$('.'+itemsClass).each(function(){
							var myId = $(this).attr('id');
							if(correctItems.indexOf(myId)== -1){
								bindClick($(this));
							}
						});
					}, delayShow+opts.animSpeed+250);
				}
			}
		};
		//removes a click from an element
		var unbindClick = function(el){
      el.unbind('click');
      el.css('cursor','default');
    };
    //adds a click to an element
    var bindClick = function(el){
      el.bind('click',handleClick);
      el.css('cursor','pointer');
    };

    //show item with different anumation/based on settings
    var showItem = function (el,id) {
    var topId = el.children('div.mg-item-top').attr('id');
      switch(opts.animType){
        default:
        case "fade":
          addInFullHTML(el,id);
          $('#'+topId).fadeOut(opts.animSpeed);
          break;
          case 'flip':
          el.flip({
            direction:opts.flipAnim,
            speed: opts.animSpeed,
            content: el.children('div.mg-item-bottom'),
            color:'#777',
            onEnd: function(){
              addInHTML(el,id);
            }
          });
          break;
          case 'scroll':
          addInFullHTML(el,id);
          $('#'+topId).animate({height: 'toggle', opacity:0.3},opts.animSpeed);
          break;
      }
    };

    // hides item with different animation/based on settings
    var hideItem = function(el){
      var topId = el.children('div.mg-item-top').attr('id');
      switch(opts.animType){
        default:
        case 'fade':
          $('#'+topId).delay(delayShow).fadeIn(opts.animSpeed, function(){
            removeInHTML(el);
          });
       break;
       case 'flip':
          setTimeout( function(){
          el.revertFlip();
          }, delayShow);
          setTimeout( function(){
           removeInHTML(el);
          }, delayShow+opts.animSpeed*4);
       break;
       case 'scroll':
          $('#'+topId).delay(delayShow).
                   animate({height: 'toggle', opacity:1},opts.animSpeed, 
                    function(){
                    removeInHTML(el);
                    });
       break;
      }
    };
    //shows and hides the correct/wrong message after an action
    var showResIcon = function(type){
      if(opts.resultIcons){
        var el;
        var time = Math.round(delayShow/3);
        if(type=='wrong'){
          el = $('div#mg-msgwrong');
        }else if(type=='correct'){
          el = $('div#mg-mgscorrect');
        }
        el.delay(time).fadeIn(time/2).delay(time/2).hide('explode', time/2);
      }
    };

    //time function
    var incTime = function(){
      numSeconds ++;
    };

    //function for adding the inner HTML
    var addInFullHTML = function(el,id){
      el.children('.mg-item-bottom')
      .children(".mgcard-show")
      .html(inHTML[id]);
    };

    var addInHTML = function(el,id){
      el.children('.mgcard-show')
      .html(inHTML[id]);
    };

    var removeInHTML = function(el){
      el.children('.mg-item-bottom').children('.mgcard-show').html('');
    };
    // MAIN CODE

    //hides the <li> items
    $(this).children('ul').hide();

    //makes the div wrapper big enough
    $(this).css({height:rowNum*(h+m)+'px'});

    /*creates an array for randomizing the items
      and creates an empty inner html array*/
    var ranArr = Array();
    for(var j=0; j< itemsNum; j++){
    inHTML[j] = '';
    ranArr.push(j);
    }
    //generates all the elements based on the data in the <li> elements
    var i=0;
    while(i<itemsNum){
    //randomizes the card - picks an item with a random key and
    //removes it from the random array
    var pick = Math.floor(Math.random()*ranArr.length);
    var j = ranArr[pick];
    ranArr.splice(pick,1);

    //gets the data from each <li> element
    var inEl = $(this).children("ul").children('li').eq(j);

    //calculates the position of each element
    var xRatio = (i+opts.colCount)%opts.colCount;
    var yRatio = Math.floor(i/opts.colCount);
    var l = xRatio*(w+m);
    var t = yRatio*(h+m);

    //adds the innerHtml to the array
    inHTML[j] = inEl.html();
    //console.log(j,inEl.html());

    //appends the cards to the element
    $(this).append('<div id="'+itemsClass+j+'" class="'+itemsClass+
      '" style="width:'+
      w+'px; height:'+h+'px; left:'+l+'px; top:'+t+'px">' +
      '<div class="mg-item-bottom"><div class="mgcard-show">'+
      '</div></div><div id="mg-item-top'+j+
      '" class="mg-item-top" style="width:'+
      w+'px; height:'+h+'px;"></div></div>');
      i++;

      //adds the element match id to the array of matches
      matches[j] = inEl.attr('class');

    }  
    //removes the initial <li> elements
    $(this).children('ul').remove();

    //adds the icons for the result after each match
    if(opts.resultIcons){
      $(this).append('<div id="mg-msgwrong"'+
      ' class="mg-notification-fly mg-notification-fly-neg"></div>'+
      '<div id="mg-msgcorrect" class="mg-notification-fly '+
      ' mg-notification-fly-pos"></div>');
    //positions the result icons in the middle of the div wrapper
    var xMid = $(this).width()/2 -
                $('div.mg-notification-fly').width()/2;
    var yMid = $(this).height()/2 -
                $('div.mg-notification-fly').height()/2 -
                opts.itemsMargin/2;
    $('div.mg-notification-fly').css({top:yMid+'px',left:xMid+'px'});
    }
    //appends game summary div if set in the opts
    if(opts.gameSummary){
    $(this).append('<div id="game-summary"><div id="gs-column1">'+
                      opts.textSummaryTitle+
                      '</div><div id="gs-column2"></div>'+
                      '<div id="gs-column3"></div></div>');
    //positions the summary div in the middle of the div wrapper
    var xMid = $(this).width()/2 -
    $('div#game-summary').width()/2;
      var yMid = $(this).height()/2 -
          $('div#game-summary').height()/2 -
          opts.itemsMargin/2;
    $('div#game-summary').css({top:yMid+'px',left:xMid+'px'});
    //adds a click event to the summary div to be removed on click
    $('div#game-summary').click(function(){
      $(this).remove();
    });
    }
    //adds the click event to each element
    $('.mg-item').click(handleClick);

 };
 $.fn.memoryGame.defaults = {itemWidth: 156, itemHeight: 156, itemsMargin:10, colCount:4, animType:'scroll', animSpeed:250, openDelay:2500, flipAnim:'rl', resultIcons:true, gameSummary:true, textSummaryTitle:'Your game summary', textSummaryClicks:'clicks', textSummaryTime:'seconds', onFinishCall:''};

})(jQuery);
